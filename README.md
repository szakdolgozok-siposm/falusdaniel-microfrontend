![banner](GitLab/Assests/Images/banner.png "Project banner")

# Micro-frontend alapú adminisztrációs felület készítése Web Components segítségével

A feladat, egy micro-frontend architektúrájú webalkalmazás készítése ügyfélszolgálati
rendszerek adminisztrálásához, Web Components technológia felhasználásával.
Vizsgálja meg, hogy mely technológiák szükségesek egy micro-frontend alapú rendszer
elkészítéséhez, elemezze azokat pozitív és negatív oldalról. Az adott technológia
melletti döntését támassza alá. A rendszer tartalmazzon egy felületet, ahol az
adminisztrátorok felkonfigurálhatják az ügyfélszolgálat digitális csatornáit, ezáltal új
kommunikációs csatornákkal bővítve azt. A rendszer biztosítsa a felhasználok számára,
hogy a digitális csatornák forgalmát statisztikák segítségével valós időben
ellenőrizhessék, nyomonkövethessék. Továbbá biztosítson egy felületet a
csoportvezetők számára, ahol az ügyintézők munkáját ellenőrizhetik. Legyen elérhető
az új ügyfelek számára egy felületet, ahol előfizethetnek a szolgáltatásokra. A rendszer
tegye lehetővé a webalkalmazás bővíthetőséget külső fejlesztők számára oly módon,
hogy adjon lehetőséget új felületi komponensek regisztrálására. A webszervereket
mikroszolgáltatás architektúra szerint készítse el. A böngészőn keresztül legyen kepés a
rendszer Web Push Notification elvű üzenetek küldésére. Végezzen továbbá elemzést a
tesztelési lehetőségekről és készítse el az egyes teszt típusokat és teszteseteket.
